
// #define is used to set constants that can be used globally (anywhere in the code).
// A #define should be formatted as shown "#define [constant name] [value]". You can see some examples below.
// #define works by each instance of the constant name is replaced by the value. So the first #define line would replace each occurence of "MIN" with "20"

#define MIN 30
#define MAX 200
#define MAX_STEP 5
#define MOTOR_PIN 3
#define POT_PIN A0
#define BUTTON_1 10
#define BUTTON_2 11
#define BUTTON_3 12

void setup() {
    pinMode(MOTOR_PIN, OUTPUT);   // Setting the motor as an output.
    digitalWrite(MOTOR_PIN, LOW); // Make sure that the motor is not on at the start.
    pinMode(POT_PIN, INPUT);      // Setting the turn knob as an input.
    Serial.begin(9600);
    Serial.println("Setup finished");
}

long in = 0;
int out = 0;

void loop() {

  in = map(analogRead(POT_PIN), 0, 1023, 0, MAX); // Reads the turn knob value and scales it to a number (0, MAX)

  int diff = in-out; // Finds what the motor is currently running at and what it is set to to make sure it is not too big of a difference, saving the motor from changing too fast.

  diff = constrain(diff, -MAX_STEP, MAX_STEP); // This is used to limit how fast the motor can turn power up or down.

  out = out + diff;

  if (out >= MIN) {
    // Once knob is turned from the off position toward the right, this will start the motor.
    analogWrite(MOTOR_PIN, out);
  }
  else {
    // Turn off motor if power is too low.
    analogWrite(MOTOR_PIN, 0);
    out = MIN - 1;
  }

  if (digitalRead(BUTTON_1)) {
    quake_1();
  }
  if (digitalRead(BUTTON_2)) {
    quake_2();
  }
  if (digitalRead(BUTTON_3)) {
    quake_3();
  }
  
  delay(50); // This will make sure the motor does not increase or decrease too quickly which could ruin the motor.
}


// You can program up to three of your own earthquakes in the functions quake_1(), quake_2() and quake_3().
// Look at quake_3() for an example on how to program a earthquake.
void quake_1() {
  
}


void quake_2() {
}


void quake_3() {

  
  // The motor can go from 0 to 255, 0 being the minimum, and 255 being the maximum. DO NOT PUT IN VALUE OVER 255, YOU COULD DESTROY THE ARDUINO.
  // These two lines will reset the motor power to 0.
  out = 0;                      
  analogWrite(MOTOR_PIN, out);

  // This will delay the start for 2000ms, 2 Seconds.
  delay(2000);

  // The change_power function can be used to change the power of the motor over a set amount of time. 
  // The first number inside the brackets is the final motor power value and the second number is how long it takes to reach that number in ms.

  
  // Because the motor power was set as 0 previously, this will rise the motor power to 100 over 2000ms
  change_power(100, 2000);

  // This will reduce the power to 50 over 2000ms
  change_power(50, 2000);

  // This will increase the power to 255 (MAXIMUM POSSIBLE POWER NUMBER! over 3000ms
  change_power(255, 3000);

  // This will hold the power last entered for 2000 ms (2 sec)
  delay(2000);

  // This will reduce the power to 0 over 5000ms.
  change_power(0, 5000);

  // When the earthquake has finished it will go back to the main loop.
}
//Do not worry about this section. This is the function created in order to change the power of the motor. DO NOT CHANGE ANYTHING IN THIS SECTION.
void change_power(int power, long time) {
  int startPower = out;
  int powerDiff = power - startPower;
  for (int i = 0; i <= 20; i++) {
    out = startPower + i*powerDiff/20;
    analogWrite(MOTOR_PIN, out);
    delay(time/20);
  }
}
