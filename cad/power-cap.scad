use <logo.scad>

$fs=0.5;
$fa=2;

union() {
    difference() {
        union() {
            cylinder(d=12, h=16, $fa=60);
            cylinder(d1=16, d2=10, h=4);
            hull() {
                translate([0, 8, 1]) rotate([0, 0, 45]) cube([2, 2, 2], center=true);
                translate([0, 4, 2.5]) rotate([0, 0, 45]) cube([2, 2, 5], center=true);
                
            }
        }
        translate([0, 0, -1]) cylinder(d=6.5, h=12+1);
        translate([0, 0, 14]) scale(1.5) logo(4);
    }
    translate([0, -1-1.6, 6.5]) cube([7, 2, 13], center=true);
}        