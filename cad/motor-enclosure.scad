md1 = 29.5;
mh = 34;

$fs = 0.8;
module main() {
    union() {
        difference() {
            l = 20;
            d = 3.8;
            d2 = 7;
            t=2;
            union() {
                // Main base plate
                minkowski() {
                    translate([0, 0, 2]) cube([2*l, 2*l, 4], center=true);
                    cylinder(d=15);
                }
                
                // Morot cylinde
                translate([0, 0, -mh]) cylinder(d=md1+2*t, h=mh);
                
                // Cable hole
                difference() {
                    translate([0, l+14/2, 0]) rotate([90, 0, 0]) cylinder(d=10, h=5);
                    translate([0, l+14/2, 0]) rotate([90, 0, 0]) cylinder(d=6, h=5);
                }
            }
            
            
            // Motor hole
            translate([0, 0, -mh+2]) cylinder(h=mh+5, d=md1);
            translate([0, 0, -mh-2]) cylinder(h=5, d=md1-t);
           
            // Motor air holes
            n=4;
            for (i=[1:n]) {
                rotate([0, 0, 180/n*i])
                translate([0, 0, -mh/2+2.5]) cube([5, md1+5, mh-5], center=true);
            }
            
            // Motor top bit cutout
            rotate([0, 0, 45]) translate([0, 0, 2+2]) 
            cube([md1+10, 4, 4], center=true); 
            rotate([0, 0, 135]) translate([0, 0, 2+2]) 
            cube([md1+10, 4, 4], center=true); 
            
            // Motor cable holes
            translate([0, 0, 2+2]) cube([md1+16, 4, 4], center=true);
            translate([md1/2+8, 0, 0]) cylinder(d=4, h=10);
            translate([-md1/2-8, 0, 0]) cylinder(d=4, h=10);
            
            // Bolt holes
            translate([l, l, -1]) cylinder(d=d, h=7);
            translate([l, -l, -1]) cylinder(d=d, h=7);
            translate([-l, l, -1]) cylinder(d=d, h=7);
            translate([-l, -l, -1]) cylinder(d=d, h=7);
            translate([l, l, -1]) cylinder(d=d2, h=3, $fa=60);
            translate([l, -l, -1]) cylinder(d=d2, h=3, $fa=60);
            translate([-l, l, -1]) cylinder(d=d2, h=3, $fa=60);
            translate([-l, -l, -1]) cylinder(d=d2, h=3, $fa=60);
            
        }
    }
}
main();

//top_bit();

module top_bit() {
    difference() {
        union() {
            rotate([0, 0, 45]) translate([0, 0, 2+1.5]) 
            cube([md1+9, 3.5, 3], center=true); 
            rotate([0, 0, 135]) translate([0, 0, 2+1.5]) 
            cube([md1+9, 3.5, 3], center=true); 
            translate([0, 0, 2]) cylinder(d=15, h=3);
        }
        cylinder(d=10, h=20);
    }
}